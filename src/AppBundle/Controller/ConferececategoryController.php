<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Conferececategory;
use AppBundle\Form\ConferececategoryType;

/**
 * Conferecne Category controller.
 *
 * @Route("/conferececategory")
 */
class ConferececategoryController extends Controller
{
    /**
     * Lists all Conferececategory entities.
     *
     * @Route("/", name="conferececategory_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $conferececategories = $em->getRepository('AppBundle:Conferececategory')->findAll();

        return $this->render('conferececategory/index.html.twig', array(
            'conferececategories' => $conferececategories,
        ));
    }

    /**
     * Creates a new Conferececategory entity.
     *
     * @Route("/new", name="conferececategory_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $conferececategory = new Conferececategory();
        $form = $this->createForm('AppBundle\Form\ConferececategoryType', $conferececategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($conferececategory);
            $em->flush();

            return $this->redirectToRoute('conferececategory_show', array('id' => $conferececategory->getId()));
        }

        return $this->render('conferececategory/new.html.twig', array(
            'conferececategory' => $conferececategory,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Conferececategory entity.
     *
     * @Route("/{id}", name="conferececategory_show")
     * @Method("GET")
     */
    public function showAction(Conferececategory $conferececategory)
    {
        $deleteForm = $this->createDeleteForm($conferececategory);

        return $this->render('conferececategory/show.html.twig', array(
            'conferececategory' => $conferececategory,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Conferececategory entity.
     *
     * @Route("/{id}/edit", name="conferececategory_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Conferececategory $conferececategory)
    {
        $deleteForm = $this->createDeleteForm($conferececategory);
        $editForm = $this->createForm('AppBundle\Form\ConferececategoryType', $conferececategory);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($conferececategory);
            $em->flush();

            return $this->redirectToRoute('conferececategory_edit', array('id' => $conferececategory->getId()));
        }

        return $this->render('conferececategory/edit.html.twig', array(
            'conferececategory' => $conferececategory,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Conferececategory entity.
     *
     * @Route("/{id}", name="conferececategory_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Conferececategory $conferececategory)
    {
        $form = $this->createDeleteForm($conferececategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($conferececategory);
            $em->flush();
        }

        return $this->redirectToRoute('conferececategory_index');
    }

    /**
     * Creates a form to delete a Conferececategory entity.
     *
     * @param Conferececategory $conferececategory The Conferececategory entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Conferececategory $conferececategory)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('conferececategory_delete', array('id' => $conferececategory->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
