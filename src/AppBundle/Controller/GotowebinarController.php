<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Gotowebinar;
use AppBundle\Form\GotowebinarType;

/**
 * Gotowebinar controller.
 *
 * @Route("/gotowebinar")
 */
class GotowebinarController extends Controller
{
    /**
     * Lists all Gotowebinar entities.
     *
     * @Route("/", name="gotowebinar_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $gotowebinars = $em->getRepository('AppBundle:Gotowebinar')->findAll();

        return $this->render('gotowebinar/index.html.twig', array(
            'gotowebinars' => $gotowebinars,
        ));
    }

    /**
     * Creates a new Gotowebinar entity.
     *
     * @Route("/new", name="gotowebinar_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $gotowebinar = new Gotowebinar();
        $form = $this->createForm('AppBundle\Form\GotowebinarType', $gotowebinar);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($gotowebinar);
            $em->flush();

            return $this->redirectToRoute('gotowebinar_show', array('id' => $gotowebinar->getId()));
        }

        return $this->render('gotowebinar/new.html.twig', array(
            'gotowebinar' => $gotowebinar,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Gotowebinar entity.
     *
     * @Route("/{id}", name="gotowebinar_show")
     * @Method("GET")
     */
    public function showAction(Gotowebinar $gotowebinar)
    {
        $deleteForm = $this->createDeleteForm($gotowebinar);

        return $this->render('gotowebinar/show.html.twig', array(
            'gotowebinar' => $gotowebinar,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Gotowebinar entity.
     *
     * @Route("/{id}/edit", name="gotowebinar_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Gotowebinar $gotowebinar)
    {
        $deleteForm = $this->createDeleteForm($gotowebinar);
        $editForm = $this->createForm('AppBundle\Form\GotowebinarType', $gotowebinar);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($gotowebinar);
            $em->flush();

            return $this->redirectToRoute('gotowebinar_edit', array('id' => $gotowebinar->getId()));
        }

        return $this->render('gotowebinar/edit.html.twig', array(
            'gotowebinar' => $gotowebinar,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Gotowebinar entity.
     *
     * @Route("/{id}", name="gotowebinar_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Gotowebinar $gotowebinar)
    {
        $form = $this->createDeleteForm($gotowebinar);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($gotowebinar);
            $em->flush();
        }

        return $this->redirectToRoute('gotowebinar_index');
    }

    /**
     * Creates a form to delete a Gotowebinar entity.
     *
     * @param Gotowebinar $gotowebinar The Gotowebinar entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Gotowebinar $gotowebinar)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('gotowebinar_delete', array('id' => $gotowebinar->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
