<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Constraints;
use AppBundle\Form\ConstraintsType;

/**
 * Constraints controller.
 *
 * @Route("/constraints")
 */
class ConstraintsController extends Controller
{
    /**
     * Lists all Constraints entities.
     *
     * @Route("/", name="constraints_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $constraints = $em->getRepository('AppBundle:Constraints')->findAll();

        return $this->render('constraints/index.html.twig', array(
            'constraints' => $constraints,
        ));
    }

    /**
     * Creates a new Constraints entity.
     *
     * @Route("/new", name="constraints_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $constraint = new Constraints();
        $form = $this->createForm('AppBundle\Form\ConstraintsType', $constraint);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($constraint);
            $em->flush();

            return $this->redirectToRoute('constraints_show', array('id' => $constraint->getId()));
        }

        return $this->render('constraints/new.html.twig', array(
            'constraint' => $constraint,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Constraints entity.
     *
     * @Route("/{id}", name="constraints_show")
     * @Method("GET")
     */
    public function showAction(Constraints $constraint)
    {
        $deleteForm = $this->createDeleteForm($constraint);

        return $this->render('constraints/show.html.twig', array(
            'constraint' => $constraint,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Constraints entity.
     *
     * @Route("/{id}/edit", name="constraints_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Constraints $constraint)
    {
        $deleteForm = $this->createDeleteForm($constraint);
        $editForm = $this->createForm('AppBundle\Form\ConstraintsType', $constraint);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($constraint);
            $em->flush();

            return $this->redirectToRoute('constraints_edit', array('id' => $constraint->getId()));
        }

        return $this->render('constraints/edit.html.twig', array(
            'constraint' => $constraint,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Constraints entity.
     *
     * @Route("/{id}", name="constraints_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Constraints $constraint)
    {
        $form = $this->createDeleteForm($constraint);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($constraint);
            $em->flush();
        }

        return $this->redirectToRoute('constraints_index');
    }

    /**
     * Creates a form to delete a Constraints entity.
     *
     * @param Constraints $constraint The Constraints entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Constraints $constraint)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('constraints_delete', array('id' => $constraint->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
