<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Webinar;
use AppBundle\Form\WebinarType;

/**
 * Webinar controller.
 *
 * @Route("/webinar")
 */
class WebinarController extends Controller
{
    /**
     * Lists all Webinar entities.
     *
     * @Route("/", name="webinar_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $webinars = $em->getRepository('AppBundle:Webinar')->findAll();

        return $this->render('webinar/index.html.twig', array(
            'webinars' => $webinars,
        ));
    }

    /**
     * Creates a new Webinar entity.
     *
     * @Route("/new", name="webinar_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $webinar = new Webinar();
        $form = $this->createForm('AppBundle\Form\WebinarType', $webinar);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($webinar);
            $em->flush();

            return $this->redirectToRoute('webinar_show', array('id' => $webinar->getId()));
        }

        return $this->render('webinar/new.html.twig', array(
            'webinar' => $webinar,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Webinar entity.
     *
     * @Route("/{id}", name="webinar_show")
     * @Method("GET")
     */
    public function showAction(Webinar $webinar)
    {
        $deleteForm = $this->createDeleteForm($webinar);

        return $this->render('webinar/show.html.twig', array(
            'webinar' => $webinar,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Webinar entity.
     *
     * @Route("/{id}/edit", name="webinar_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Webinar $webinar)
    {
        $deleteForm = $this->createDeleteForm($webinar);
        $editForm = $this->createForm('AppBundle\Form\WebinarType', $webinar);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($webinar);
            $em->flush();

            return $this->redirectToRoute('webinar_edit', array('id' => $webinar->getId()));
        }

        return $this->render('webinar/edit.html.twig', array(
            'webinar' => $webinar,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Webinar entity.
     *
     * @Route("/{id}", name="webinar_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Webinar $webinar)
    {
        $form = $this->createDeleteForm($webinar);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($webinar);
            $em->flush();
        }

        return $this->redirectToRoute('webinar_index');
    }

    /**
     * Creates a form to delete a Webinar entity.
     *
     * @param Webinar $webinar The Webinar entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Webinar $webinar)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('webinar_delete', array('id' => $webinar->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
