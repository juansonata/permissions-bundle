<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Webinarcategory;
use AppBundle\Form\WebinarcategoryType;

/**
 * Webinarcategory controller.
 *
 * @Route("/webinarcategory")
 */
class WebinarcategoryController extends Controller
{
    /**
     * Lists all Webinarcategory entities.
     *
     * @Route("/", name="webinarcategory_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $webinarcategories = $em->getRepository('AppBundle:Webinarcategory')->findAll();

        return $this->render('webinarcategory/index.html.twig', array(
            'webinarcategories' => $webinarcategories,
        ));
    }

    /**
     * Creates a new Webinarcategory entity.
     *
     * @Route("/new", name="webinarcategory_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $webinarcategory = new Webinarcategory();
        $form = $this->createForm('AppBundle\Form\WebinarcategoryType', $webinarcategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($webinarcategory);
            $em->flush();

            return $this->redirectToRoute('webinarcategory_show', array('id' => $webinarcategory->getId()));
        }

        return $this->render('webinarcategory/new.html.twig', array(
            'webinarcategory' => $webinarcategory,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Webinarcategory entity.
     *
     * @Route("/{id}", name="webinarcategory_show")
     * @Method("GET")
     */
    public function showAction(Webinarcategory $webinarcategory)
    {
        $deleteForm = $this->createDeleteForm($webinarcategory);

        return $this->render('webinarcategory/show.html.twig', array(
            'webinarcategory' => $webinarcategory,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Webinarcategory entity.
     *
     * @Route("/{id}/edit", name="webinarcategory_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Webinarcategory $webinarcategory)
    {
        $deleteForm = $this->createDeleteForm($webinarcategory);
        $editForm = $this->createForm('AppBundle\Form\WebinarcategoryType', $webinarcategory);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($webinarcategory);
            $em->flush();

            return $this->redirectToRoute('webinarcategory_edit', array('id' => $webinarcategory->getId()));
        }

        return $this->render('webinarcategory/edit.html.twig', array(
            'webinarcategory' => $webinarcategory,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Webinarcategory entity.
     *
     * @Route("/{id}", name="webinarcategory_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Webinarcategory $webinarcategory)
    {
        $form = $this->createDeleteForm($webinarcategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($webinarcategory);
            $em->flush();
        }

        return $this->redirectToRoute('webinarcategory_index');
    }

    /**
     * Creates a form to delete a Webinarcategory entity.
     *
     * @param Webinarcategory $webinarcategory The Webinarcategory entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Webinarcategory $webinarcategory)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('webinarcategory_delete', array('id' => $webinarcategory->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
