<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Eventcategory;
use AppBundle\Form\EventcategoryType;

/**
 * Eventcategory controller.
 *
 * @Route("/eventcategory")
 */
class EventcategoryController extends Controller
{
    /**
     * Lists all Eventcategory entities.
     *
     * @Route("/", name="eventcategory_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $eventcategories = $em->getRepository('AppBundle:Eventcategory')->findAll();

        return $this->render('eventcategory/index.html.twig', array(
            'eventcategories' => $eventcategories,
        ));
    }

    /**
     * Creates a new Eventcategory entity.
     *
     * @Route("/new", name="eventcategory_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $eventcategory = new Eventcategory();
        $form = $this->createForm('AppBundle\Form\EventcategoryType', $eventcategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($eventcategory);
            $em->flush();

            return $this->redirectToRoute('eventcategory_show', array('id' => $eventcategory->getId()));
        }

        return $this->render('eventcategory/new.html.twig', array(
            'eventcategory' => $eventcategory,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Eventcategory entity.
     *
     * @Route("/{id}", name="eventcategory_show")
     * @Method("GET")
     */
    public function showAction(Eventcategory $eventcategory)
    {
        $deleteForm = $this->createDeleteForm($eventcategory);

        return $this->render('eventcategory/show.html.twig', array(
            'eventcategory' => $eventcategory,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Eventcategory entity.
     *
     * @Route("/{id}/edit", name="eventcategory_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Eventcategory $eventcategory)
    {
        $deleteForm = $this->createDeleteForm($eventcategory);
        $editForm = $this->createForm('AppBundle\Form\EventcategoryType', $eventcategory);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($eventcategory);
            $em->flush();

            return $this->redirectToRoute('eventcategory_edit', array('id' => $eventcategory->getId()));
        }

        return $this->render('eventcategory/edit.html.twig', array(
            'eventcategory' => $eventcategory,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Eventcategory entity.
     *
     * @Route("/{id}", name="eventcategory_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Eventcategory $eventcategory)
    {
        $form = $this->createDeleteForm($eventcategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($eventcategory);
            $em->flush();
        }

        return $this->redirectToRoute('eventcategory_index');
    }

    /**
     * Creates a form to delete a Eventcategory entity.
     *
     * @param Eventcategory $eventcategory The Eventcategory entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Eventcategory $eventcategory)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('eventcategory_delete', array('id' => $eventcategory->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
