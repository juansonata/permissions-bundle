<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\ContainerGroup;
use AppBundle\Form\ContainerGroupType;

/**
 * ContainerGroup controller.
 *
 * @Route("/containergroup")
 */
class ContainerGroupController extends Controller
{
    /**
     * Lists all ContainerGroup entities.
     *
     * @Route("/", name="containergroup_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $containerGroups = $em->getRepository('AppBundle:ContainerGroup')->findAll();

        return $this->render('containergroup/index.html.twig', array(
            'containerGroups' => $containerGroups,
        ));
    }

    /**
     * Creates a new ContainerGroup entity.
     *
     * @Route("/new", name="containergroup_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $containerGroup = new ContainerGroup();
        $form = $this->createForm('AppBundle\Form\ContainerGroupType', $containerGroup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($containerGroup);
            $em->flush();

            return $this->redirectToRoute('containergroup_show', array('id' => $containerGroup->getId()));
        }

        return $this->render('containergroup/new.html.twig', array(
            'containerGroup' => $containerGroup,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ContainerGroup entity.
     *
     * @Route("/{id}", name="containergroup_show")
     * @Method("GET")
     */
    public function showAction(ContainerGroup $containerGroup)
    {
        $deleteForm = $this->createDeleteForm($containerGroup);

        return $this->render('containergroup/show.html.twig', array(
            'containerGroup' => $containerGroup,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing ContainerGroup entity.
     *
     * @Route("/{id}/edit", name="containergroup_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ContainerGroup $containerGroup)
    {
        $deleteForm = $this->createDeleteForm($containerGroup);
        $editForm = $this->createForm('AppBundle\Form\ContainerGroupType', $containerGroup);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($containerGroup);
            $em->flush();

            return $this->redirectToRoute('containergroup_edit', array('id' => $containerGroup->getId()));
        }

        return $this->render('containergroup/edit.html.twig', array(
            'containerGroup' => $containerGroup,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a ContainerGroup entity.
     *
     * @Route("/{id}", name="containergroup_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, ContainerGroup $containerGroup)
    {
        $form = $this->createDeleteForm($containerGroup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($containerGroup);
            $em->flush();
        }

        return $this->redirectToRoute('containergroup_index');
    }

    /**
     * Creates a form to delete a ContainerGroup entity.
     *
     * @param ContainerGroup $containerGroup The ContainerGroup entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ContainerGroup $containerGroup)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('containergroup_delete', array('id' => $containerGroup->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
