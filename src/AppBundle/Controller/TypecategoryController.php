<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Typecategory;
use AppBundle\Form\TypecategoryType;

/**
 * Typecategory controller.
 *
 * @Route("/typecategory")
 */
class TypecategoryController extends Controller
{
    /**
     * Lists all Typecategory entities.
     *
     * @Route("/", name="typecategory_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $typecategories = $em->getRepository('AppBundle:Typecategory')->findAll();

        return $this->render('typecategory/index.html.twig', array(
            'typecategories' => $typecategories,
        ));
    }

    /**
     * Creates a new Typecategory entity.
     *
     * @Route("/new", name="typecategory_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $typecategory = new Typecategory();
        $form = $this->createForm('AppBundle\Form\TypecategoryType', $typecategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($typecategory);
            $em->flush();

            return $this->redirectToRoute('typecategory_show', array('id' => $typecategory->getId()));
        }

        return $this->render('typecategory/new.html.twig', array(
            'typecategory' => $typecategory,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Typecategory entity.
     *
     * @Route("/{id}", name="typecategory_show")
     * @Method("GET")
     */
    public function showAction(Typecategory $typecategory)
    {
        $deleteForm = $this->createDeleteForm($typecategory);

        return $this->render('typecategory/show.html.twig', array(
            'typecategory' => $typecategory,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Typecategory entity.
     *
     * @Route("/{id}/edit", name="typecategory_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Typecategory $typecategory)
    {
        $deleteForm = $this->createDeleteForm($typecategory);
        $editForm = $this->createForm('AppBundle\Form\TypecategoryType', $typecategory);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($typecategory);
            $em->flush();

            return $this->redirectToRoute('typecategory_edit', array('id' => $typecategory->getId()));
        }

        return $this->render('typecategory/edit.html.twig', array(
            'typecategory' => $typecategory,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Typecategory entity.
     *
     * @Route("/{id}", name="typecategory_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Typecategory $typecategory)
    {
        $form = $this->createDeleteForm($typecategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($typecategory);
            $em->flush();
        }

        return $this->redirectToRoute('typecategory_index');
    }

    /**
     * Creates a form to delete a Typecategory entity.
     *
     * @param Typecategory $typecategory The Typecategory entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Typecategory $typecategory)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('typecategory_delete', array('id' => $typecategory->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
