<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Actionplan;
use AppBundle\Form\ActionplanType;

/**
 * Actionplan controller.
 *
 * @Route("/actionplan")
 */
class ActionplanController extends Controller
{
    /**
     * Lists all Actionplan entities.
     *
     * @Route("/", name="actionplan_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $actionplans = $em->getRepository('AppBundle:Actionplan')->findAll();

        return $this->render('actionplan/index.html.twig', array(
            'actionplans' => $actionplans,
        ));
    }

    /**
     * Creates a new Actionplan entity.
     *
     * @Route("/new", name="actionplan_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $actionplan = new Actionplan();
        $form = $this->createForm('AppBundle\Form\ActionplanType', $actionplan);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($actionplan);
            $em->flush();

            return $this->redirectToRoute('actionplan_show', array('id' => $actionplan->getId()));
        }

        return $this->render('actionplan/new.html.twig', array(
            'actionplan' => $actionplan,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Actionplan entity.
     *
     * @Route("/{id}", name="actionplan_show")
     * @Method("GET")
     */
    public function showAction(Actionplan $actionplan)
    {
        $deleteForm = $this->createDeleteForm($actionplan);

        return $this->render('actionplan/show.html.twig', array(
            'actionplan' => $actionplan,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Actionplan entity.
     *
     * @Route("/{id}/edit", name="actionplan_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Actionplan $actionplan)
    {
        $deleteForm = $this->createDeleteForm($actionplan);
        $editForm = $this->createForm('AppBundle\Form\ActionplanType', $actionplan);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($actionplan);
            $em->flush();

            return $this->redirectToRoute('actionplan_edit', array('id' => $actionplan->getId()));
        }

        return $this->render('actionplan/edit.html.twig', array(
            'actionplan' => $actionplan,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Actionplan entity.
     *
     * @Route("/{id}", name="actionplan_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Actionplan $actionplan)
    {
        $form = $this->createDeleteForm($actionplan);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($actionplan);
            $em->flush();
        }

        return $this->redirectToRoute('actionplan_index');
    }

    /**
     * Creates a form to delete a Actionplan entity.
     *
     * @param Actionplan $actionplan The Actionplan entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Actionplan $actionplan)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('actionplan_delete', array('id' => $actionplan->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
