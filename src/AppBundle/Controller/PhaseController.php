<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Phase;
use AppBundle\Form\PhaseType;

/**
 * Phase controller.
 *
 * @Route("/phase")
 */
class PhaseController extends Controller
{
    /**
     * Lists all Phase entities.
     *
     * @Route("/", name="phase_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $phases = $em->getRepository('AppBundle:Phase')->findAll();

        return $this->render('phase/index.html.twig', array(
            'phases' => $phases,
        ));
    }

    /**
     * Creates a new Phase entity.
     *
     * @Route("/new", name="phase_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $phase = new Phase();
        $form = $this->createForm('AppBundle\Form\PhaseType', $phase);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($phase);
            $em->flush();

            return $this->redirectToRoute('phase_show', array('id' => $phase->getId()));
        }

        return $this->render('phase/new.html.twig', array(
            'phase' => $phase,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Phase entity.
     *
     * @Route("/{id}", name="phase_show")
     * @Method("GET")
     */
    public function showAction(Phase $phase)
    {
        $deleteForm = $this->createDeleteForm($phase);

        return $this->render('phase/show.html.twig', array(
            'phase' => $phase,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Phase entity.
     *
     * @Route("/{id}/edit", name="phase_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Phase $phase)
    {
        $deleteForm = $this->createDeleteForm($phase);
        $editForm = $this->createForm('AppBundle\Form\PhaseType', $phase);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($phase);
            $em->flush();

            return $this->redirectToRoute('phase_edit', array('id' => $phase->getId()));
        }

        return $this->render('phase/edit.html.twig', array(
            'phase' => $phase,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Phase entity.
     *
     * @Route("/{id}", name="phase_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Phase $phase)
    {
        $form = $this->createDeleteForm($phase);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($phase);
            $em->flush();
        }

        return $this->redirectToRoute('phase_index');
    }

    /**
     * Creates a form to delete a Phase entity.
     *
     * @param Phase $phase The Phase entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Phase $phase)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('phase_delete', array('id' => $phase->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
