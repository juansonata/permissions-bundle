<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Permission;
use AppBundle\Form\PermissionType;

/**
 * Permission controller.
 *
 * @Route("/permission")
 */
class PermissionController extends Controller
{
    /**
     * Lists all Permission entities.
     *
     * @Route("/", name="permission_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $permissions = $em->getRepository('AppBundle:Permission')->findAll();

        return $this->render('permission/index.html.twig', array(
            'permissions' => $permissions,
        ));
    }

    /**
     * Creates a new Permission entity.
     *
     * @Route("/new", name="permission_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $permission = new Permission();
        $form = $this->createForm('AppBundle\Form\PermissionType', $permission);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($permission);
            $em->flush();

            return $this->redirectToRoute('permission_show', array('id' => $permission->getId()));
        }

        return $this->render('permission/new.html.twig', array(
            'permission' => $permission,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Permission entity.
     *
     * @Route("/{id}", name="permission_show")
     * @Method("GET")
     */
    public function showAction(Permission $permission)
    {
        $deleteForm = $this->createDeleteForm($permission);

        return $this->render('permission/show.html.twig', array(
            'permission' => $permission,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Permission entity.
     *
     * @Route("/{id}/edit", name="permission_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Permission $permission)
    {
        $deleteForm = $this->createDeleteForm($permission);
        $editForm = $this->createForm('AppBundle\Form\PermissionType', $permission);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($permission);
            $em->flush();

            return $this->redirectToRoute('permission_edit', array('id' => $permission->getId()));
        }

        return $this->render('permission/edit.html.twig', array(
            'permission' => $permission,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Permission entity.
     *
     * @Route("/{id}", name="permission_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Permission $permission)
    {
        $form = $this->createDeleteForm($permission);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($permission);
            $em->flush();
        }

        return $this->redirectToRoute('permission_index');
    }

    /**
     * Creates a form to delete a Permission entity.
     *
     * @param Permission $permission The Permission entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Permission $permission)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('permission_delete', array('id' => $permission->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
