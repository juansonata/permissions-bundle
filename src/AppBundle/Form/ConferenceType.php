<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConferenceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('slug')
            ->add('description')
            ->add('status')
            ->add('publishedAt', 'datetime')
            ->add('expiresAt', 'datetime')
            ->add('imageUrl')
            ->add('date', 'datetime')
            ->add('location')
            ->add('phone')
            ->add('address')
            ->add('city')
            ->add('state')
            ->add('zip')
            ->add('country')
            ->add('hotelInfo')
            ->add('classroomTimes')
            ->add('conferececategory')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Conference'
        ));
    }
}
