<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Permission
 *
 * @ORM\Table(name="permission")
 * @ORM\Entity
 */
class Permission
{
    /**
     * @var string
     *
     * @ORM\Column(name="module_key", type="string", length=200, nullable=true)
     */
    private $moduleKey;

    /**
     * @var integer
     *
     * @ORM\Column(name="object_id", type="integer", nullable=true)
     */
    private $objectId;

    /**
     * @var integer
     *
     * @ORM\Column(name="tag_id", type="integer", nullable=true)
     */
    private $tagId;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User", inversedBy="permission")
     * @ORM\JoinTable(name="user_permission",
     *   joinColumns={
     *     @ORM\JoinColumn(name="permission_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     *   }
     * )
     */
    private $user;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Typecategory", mappedBy="permission")
     */
    private $typecategory;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Groups", inversedBy="permission")
     * @ORM\JoinTable(name="group_permission",
     *   joinColumns={
     *     @ORM\JoinColumn(name="permission_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     *   }
     * )
     */
    private $group;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->user = new \Doctrine\Common\Collections\ArrayCollection();
        $this->typecategory = new \Doctrine\Common\Collections\ArrayCollection();
        $this->group = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set module key
     *
     * @param string $moduleKey
     * @return Permission
     */
    public function setModuleKey($moduleKey)
    {
        $this->moduleKey = $moduleKey;

        return $this;
    }

    /**
     * Get module key
     *
     * @return string 
     */
    public function getModuleKey()
    {
        return $this->moduleKey;
    }

    /**
     * Set object id
     *
     * @param string $objectId
     * @return Permission
     */
    public function setObjectId($objectId)
    {
        $this->objectId = $objectId;

        return $this;
    }

    /**
     * Get object id
     *
     * @return string 
     */
    public function getObjectId()
    {
        return $this->objectId;
    }

    /**
     * Set tag id
     *
     * @param string $tagId
     * @return Permission
     */
    public function setTagId($tagId)
    {
        $this->tagId = $tagId;

        return $this;
    }

    /**
     * Get tag id
     *
     * @return string 
     */
    public function getTagId()
    {
        return $this->tagId;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Permission
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param ArrayCollection $user
     * @return Permission
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param ArrayCollection $typecategory
     * @return Permission
     */
    public function setTypecategory($typecategory)
    {
        $this->typecategory = $typecategory;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getTypecategory()
    {
        return $this->typecategory;
    }

    /**
     * @param ArrayCollection $group
     * @return Permission
     */
    public function setGroup($group)
    {
        $this->group = $group;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getGroup()
    {
        return $this->group;
    }

    public function __toString()
    {
        return strval($this->id);
    }
}