<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Webinar
 *
 * @ORM\Table(name="webinar", indexes={@ORM\Index(name="fk_webinars_gotowebinars1_idx", columns={"gotowebinar_id"})})
 * @ORM\Entity
 */
class Webinar
{
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=100, nullable=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="published_at", type="datetime", nullable=true)
     */
    private $publishedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expires_at", type="datetime", nullable=true)
     */
    private $expiresAt;

    /**
     * @var string
     *
     * @ORM\Column(name="image_url", type="text", length=65535, nullable=true)
     */
    private $imageUrl;

    /**
     * @var integer
     *
     * @ORM\Column(name="learning_hours", type="integer", nullable=true)
     */
    private $learningHours;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Gotowebinar
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Gotowebinar")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="gotowebinar_id", referencedColumnName="id")
     * })
     */
    private $gotowebinar;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Webinarcategory", inversedBy="webinar")
     * @ORM\JoinTable(name="webinar_webinarcategory",
     *   joinColumns={
     *     @ORM\JoinColumn(name="webinar_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="webinarcategory_id", referencedColumnName="id")
     *   }
     * )
     */
    private $webinarcategory;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->webinarcategory = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Webinar
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Webinar
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Webinar
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Webinar
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }   

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set published at
     *
     * @param Datetime $publishedAt
     * @return Webinar
     */
    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }   

    /**
     * Get published at
     *
     * @return Datetime 
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * Set expires at
     *
     * @param Datetime $expiresAt
     * @return Webinar
     */
    public function setExpiresAt($expiresAt)
    {
        $this->expiresAt = $expiresAt;

        return $this;
    }   

    /**
     * Get expires at
     *
     * @return Datetime 
     */
    public function getExpiresAt()
    {
        return $this->expiresAt;
    }

    /**
     * Set image url
     *
     * @param string $imageUrl
     * @return Webinar
     */
    public function setImageUrl($imageUrl)
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }   

    /**
     * Get image url
     *
     * @return string 
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    /**
     * Set learning hours
     *
     * @param Integer $learningHours
     * @return Webinar
     */
    public function setLearningHours($learningHours)
    {
        $this->learningHours = $learningHours;

        return $this;
    }   

    /**
     * Get learning hours
     *
     * @return Integer 
     */
    public function getLearningHours()
    {
        return $this->learningHours;
    }

    /**
     * @param ArrayCollection $webinarcategory
     * @return Webinar
     */
    public function setWebinarcategory($webinarcategory)
    {
        $this->webinarcategory = $webinarcategory;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getWebinarcategory()
    {
        return $this->webinarcategory;
    }

    public function __toString()
    {
        return strval($this->id);
    }
}