<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phase
 *
 * @ORM\Table(name="phase")
 * @ORM\Entity
 */
class Phase
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Program", mappedBy="phase")
     */
    private $program;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Course", inversedBy="phase")
     * @ORM\JoinTable(name="phase_course",
     *   joinColumns={
     *     @ORM\JoinColumn(name="phase_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="course_id", referencedColumnName="id")
     *   }
     * )
     */
    private $course;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->program = new \Doctrine\Common\Collections\ArrayCollection();
        $this->course = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Phase
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param ArrayCollection $program
     * @return Phase
     */
    public function setProgram($program)
    {
        $this->program = $program;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getProgram()
    {
        return $this->program;
    }

    /**
     * @param ArrayCollection $course
     * @return Phase
     */
    public function setCourse($course)
    {
        $this->course = $course;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getCourse()
    {
        return $this->course;
    }

    public function __toString()
    {
        return strval($this->id);
    }
}