<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Task
 *
 * @ORM\Table(name="task", indexes={@ORM\Index(name="fk_task_task1_idx", columns={"task_id"}), @ORM\Index(name="fk_task_actionplan1_idx", columns={"actionplan_id"})})
 * @ORM\Entity
 */
class Task
{
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=45, nullable=true)
     */
    private $title;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Actionplan
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Actionplan")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="actionplan_id", referencedColumnName="id")
     * })
     */
    private $actionplan;

    /**
     * @var \AppBundle\Entity\Task
     * 
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Task")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="task_id", referencedColumnName="id")
     * })
     */
    private $task;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Task
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param ArrayCollection $task
     * @return Task
     */
    public function setTask($task)
    {
        $this->task = $task;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getTask()
    {
        return $this->task;
    }

    public function __toString()
    {
        return strval($this->id);
    }
}
