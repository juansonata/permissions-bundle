<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Gotowebinar
 *
 * @ORM\Table(name="gotowebinar")
 * @ORM\Entity
 */
class Gotowebinar
{
    /**
     * @var string
     *
     * @ORM\Column(name="key", type="string", length=45, nullable=true)
     */
    private $key;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=45, nullable=true)
     */
    private $title;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_time", type="datetime", nullable=true)
     */
    private $startTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_time", type="datetime", nullable=true)
     */
    private $endTime;

    /**
     * @var string
     *
     * @ORM\Column(name="speaker_name", type="string", length=45, nullable=true)
     */
    private $speakerName;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set key
     *
     * @param string $key
     * @return Gotowebinar
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Gotowebinar
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set start time
     *
     * @param Datetime $startTime
     * @return Gotowebinar
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * Get start time
     *
     * @return Datetime 
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * Set end time
     *
     * @param Datetime $endTime
     * @return Gotowebinar
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;

        return $this;
    }

    /**
     * Get end time
     *
     * @return Datetime 
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * Set speaker name
     *
     * @param string $speakerName
     * @return Gotowebinar
     */
    public function setSpeakerName($speakerName)
    {
        $this->speakerName = $speakerName;

        return $this;
    }

    /**
     * Get speaker name
     *
     * @return string 
     */
    public function getSpeakerName()
    {
        return $this->speakerName;
    }

    public function __toString()
    {
        return strval($this->id);
    }
}