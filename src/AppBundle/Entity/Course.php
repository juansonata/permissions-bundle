<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Course
 *
 * @ORM\Table(name="course")
 * @ORM\Entity
 */
class Course
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Phase", mappedBy="course")
     */
    private $phase;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Program", inversedBy="course")
     * @ORM\JoinTable(name="course_program",
     *   joinColumns={
     *     @ORM\JoinColumn(name="course_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="program_id", referencedColumnName="id")
     *   }
     * )
     */
    private $program;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->phase = new \Doctrine\Common\Collections\ArrayCollection();
        $this->program = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Course
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param ArrayCollection $phase
     * @return Course
     */
    public function setPhase($phase)
    {
        $this->phase = $phase;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getPhase()
    {
        return $this->phase;
    }

    /**
     * @param ArrayCollection $program
     * @return Course
     */
    public function setProgram($program)
    {
        $this->program = $program;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getProgram()
    {
        return $this->program;
    }

    public function __toString()
    {
        return strval($this->id);
    }
}
