<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Constraints
 *
 * @ORM\Table(name="constraints", indexes={@ORM\Index(name="fk_permissions_prereqs_permissions1_idx", columns={"permission_id"})})
 * @ORM\Entity
 */
class Constraints
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="permission_category_id", type="integer", nullable=true)
     */
    private $permissionCategoryId;

    /**
     * @var string
     *
     * @ORM\Column(name="prereq_id", type="string", length=45, nullable=true)
     */
    private $prereqId;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer", nullable=true)
     */
    private $quantity;

    /**
     * @var string
     *
     * @ORM\Column(name="action", type="string", length=250, nullable=false)
     */
    private $action;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Permission
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Permission")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="permission_id", referencedColumnName="id")
     * })
     */
    private $permission;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User", mappedBy="constraint")
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->user = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Constraints
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set permission category id
     *
     * @param Integer $permissionCategoryId
     * @return Constraints
     */
    public function setPermissionCategoryId($permissionCategoryId)
    {
        $this->permissionCategoryId = $permissionCategoryId;

        return $this;
    }

    /**
     * Get permission category id
     *
     * @return Integer 
     */
    public function getPermissionCategoryId()
    {
        return $this->permissionCategoryId;
    }

    /**
     * Set prereq id
     *
     * @param Integer $prereqId
     * @return Constraints
     */
    public function setPrereqId($prereqId)
    {
        $this->prereqId = $prereqId;

        return $this;
    }

    /**
     * Get prereq id
     *
     * @return Integer 
     */
    public function getPrereqId()
    {
        return $this->prereqId;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Constraints
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set quantity
     *
     * @param Integer $quantity
     * @return Constraints
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return Integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set action
     *
     * @param string $action
     * @return Constraints
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return string 
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Constraints
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param ArrayCollection $permission
     * @return Chapter
     */
    public function setPermission($permission)
    {
        $this->permission = $permission;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getPermission()
    {
        return $this->permission;
    }

    /**
     * @param ArrayCollection $user
     * @return Chapter
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getUser()
    {
        return $this->user;
    }

    public function __toString()
    {
        return strval($this->id);
    }
}