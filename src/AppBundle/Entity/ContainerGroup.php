<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContainerGroup
 *
 * @ORM\Table(name="container_group")
 * @ORM\Entity
 */
class ContainerGroup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="group_id", type="integer", nullable=true)
     */
    private $groupId;

    /**
     * @var string
     *
     * @ORM\Column(name="ContainerGroupList", type="string", length=255, nullable=true)
     */
    private $containergrouplist;

    /**
     * @var string
     *
     * @ORM\Column(name="AnnexedGroupList", type="string", length=255, nullable=true)
     */
    private $annexedgrouplist;

    /**
     * @var \AppBundle\Entity\Groups
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Groups")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id", referencedColumnName="id")
     * })
     */
    private $id;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set group id
     *
     * @param Integer $groupId
     * @return Constraints
     */
    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;

        return $this;
    }

    /**
     * Get group id
     *
     * @return Integer 
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * Set container group list
     *
     * @param string $containergrouplist
     * @return Constraints
     */
    public function setContainergrouplist($containergrouplist)
    {
        $this->containergrouplist = $containergrouplist;

        return $this;
    }

    /**
     * Get container group list
     *
     * @return string 
     */
    public function getContainergrouplist()
    {
        return $this->containergrouplist;
    }

    /**
     * Set annexed group list
     *
     * @param string $annexedgrouplist
     * @return Constraints
     */
    public function setAnnexedgrouplist($annexedgrouplist)
    {
        $this->annexedgrouplist = $annexedgrouplist;

        return $this;
    }

    /**
     * Get annexed group list
     *
     * @return string 
     */
    public function getAnnexedgrouplist()
    {
        return $this->annexedgrouplist;
    }

    public function __toString()
    {
        return strval($this->id);
    }
}